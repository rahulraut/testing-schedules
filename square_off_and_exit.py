import os
import logging
import math

import sys
import time
from datetime import datetime as dt
from time import sleep

import pandas as pd
import requests
from bs4 import BeautifulSoup
from smartapi import SmartConnect

# import config as config

print(f"square off value:: {os.getenv('SQUARE_OFF')}")

print(f"square off value:: {os.environ.get('SQUARE_OFF')}")
print(f"angel username value value:: {os.environ.get('ANGEL_USERNAME')}")

import pytz

tz = pytz.timezone('Asia/Kolkata')
kolkata_zone = dt.now(tz)
print(f'start time of script :: {kolkata_zone}')


today = dt.today()
logging.basicConfig(filename=f"{today.year}-{today.month}-{today.day}.log",
                    format='%(asctime)s %(message)s',
                    level=logging.INFO)


# print = logging.info

username = os.environ.get('ANGEL_USERNAME')
pwd = os.environ.get('ANGEL_PWD')
apikey = os.environ.get('API_KEY')

pd.options.display.max_rows = 220
pd.options.display.max_columns = 50
pd.set_option('display.width', 1000)


obj = SmartConnect(api_key=apikey)
session_info = obj.generateSession(username, pwd)
# print(session_info['data']['refreshToken'])
print(
    f"login status :: '{session_info['status']}', message :: '{session_info['message']}', errorcode :: '{session_info['errorcode']}'")
# print(session_info['data']['products'])


def cancel_all_intraday_orders():
    orders = obj.orderBook()
    orders_df = pd.DataFrame(orders['data'])
    if not orders_df.empty:
        orders_df = orders_df[['variety', 'ordertype', 'producttype', 'price', 'triggerprice',
                            'orderid', 'tradingsymbol', 'symboltoken', 'status', 'orderstatus', 'quantity']]
        print(orders_df)
        orders_df = orders_df[orders_df['producttype'] == 'INTRADAY']
        print(orders_df)
        for order in orders_df.itertuples():
            print(order)
            obj.cancelOrder(order.orderid, order.variety)
        sleep(0.2)


def exit_all_intraday_positions():

    positions = obj.position()
    positions_df = pd.DataFrame(positions['data'])
    if not positions_df.empty:
        positions_df = positions_df[['symboltoken', 'tradingsymbol', 'instrumenttype', 'producttype',
                                    'buyavgprice', 'sellavgprice', 'netqty', 'pnl', 'realised', 'unrealised', 'ltp', 'lotsize']]
        print(positions_df)

        intraday_positions = positions_df[positions_df['producttype'] == 'INTRADAY']
        for position in intraday_positions.itertuples():
            print(position)
            if int(position.netqty) < 0:
                place_market_order(
                    position.symboltoken, position.tradingsymbol, int(position.netqty), 'BUY')
            elif int(position.netqty) > 0:
                place_market_order(
                    position.symboltoken, position.tradingsymbol, int(position.netqty), 'SELL')


"""
Pandas(Index=1, symboltoken='3045', symbolname='SBIN', instrumenttype='', priceden='1.00', pricenum='1.00', genden='1.00', gennum='1.00', 
precision='2', multiplier='-1', boardlotsize='1', exchange='NSE', producttype='INTRADAY', tradingsymbol='SBIN-EQ', symbolgroup='EQ',
strikeprice='-1', optiontype='', expirydate='', lotsize='1', cfbuyqty='0', cfsellqty='0', cfbuyamount='0.00', cfsellamount='0.00', 
buyavgprice='515.20', sellavgprice='0.00', avgnetprice='515.20', netvalue='-515.20', netqty='1', totalbuyvalue='515.20', 
totalsellvalue='0.00', cfbuyavgprice='0.00', cfsellavgprice='0.00', totalbuyavgprice='515.20', totalsellavgprice='0.00', netprice='515.20',
 buyqty='1', sellqty='0', buyamount='515.20', sellamount='0.00', pnl='0.25', realised='-0.00', unrealised='0.25', ltp='515.45', close='513.95')
"""


def print_order_book():
    orders = obj.orderBook()
    orders_df = pd.DataFrame(orders['data'])
    if not orders_df.empty:
        orders_df = orders_df[['variety', 'ordertype', 'producttype', 'transactiontype', 'price',
                            'triggerprice', 'orderid', 'tradingsymbol', 'symboltoken', 'status', 'orderstatus', 'quantity']]
        print(orders_df)
        orders_df = orders_df[orders_df['producttype'] == 'INTRADAY']
        print(orders_df)


def place_market_order(token, symbol, qty, buy_sell):
    try:
        orderparams = {
            "variety": 'NORMAL',
            "tradingsymbol": symbol,
            "symboltoken": token,
            "transactiontype": buy_sell,
            "exchange": "NSE",
            "ordertype": "MARKET",
            "producttype": "INTRADAY",
            "duration": "DAY",
            "price": 0.0,
            "quantity": qty
        }
        print(orderparams)
        orderId = obj.placeOrder(orderparams)
        print("Position Exited and id is: {}".format(orderId))
    except Exception as e:
        print("Issue with position exit: {}".format(e.message))
    finally:
        sleep(0.2)


# run at 15:05 pm since there is sometimes issue if we run before
if __name__ == "__main__":
    # print positions

    # cancel_all_intraday_orders
    cancel_all_intraday_orders()

    # exit 
    exit_all_intraday_positions()

    # print order book
    print_order_book()

