import datetime
import logging
import math
import os
import sys
import time
from datetime import datetime as dt
from time import sleep
import pytz

import pandas as pd
import requests
from bs4 import BeautifulSoup
from smartapi import SmartConnect

# import config as config

today = dt.today()
logging.basicConfig(filename=f"{today.year}-{today.month}-{today.day}.log",
                    format='%(asctime)s %(message)s',
                    level=logging.DEBUG)


print(f"place order value:: {os.environ.get('PLACE_ORDER')}")
print(f"angel username value value:: {os.environ.get('ANGEL_USERNAME')}")

tz = pytz.timezone('Asia/Kolkata')
kolkata_zone = dt.now(tz)
print(f'start time of script :: {kolkata_zone}')

print = logging.info


username = os.environ.get('ANGEL_USERNAME')
pwd = os.environ.get('ANGEL_PWD')
apikey = os.environ.get('API_KEY')

pd.options.display.max_rows = 220
pd.options.display.max_columns = 20
pd.set_option('display.width', 1000)

obj = SmartConnect(api_key=apikey)
session_info = obj.generateSession(username, pwd)
# print(session_info['data']['refreshToken'])
print(
    f"login status :: '{session_info['status']}', message :: '{session_info['message']}', errorcode :: '{session_info['errorcode']}'")
# print(session_info['data']['products'])

def place_bo_angel(token, symbol, qty, buy_sell, price, variety='NORMAL', triggerprice=0, squareoff=0, stoploss=0):
    try:
        orderparams = {
            "variety": variety,
            "tradingsymbol": symbol,
            "symboltoken": token,
            "transactiontype": buy_sell,
            "exchange": "NSE",
            "ordertype": "LIMIT",
            "producttype": "INTRADAY",
            "duration": "DAY",
            "price": price,
            # difference between price and squareoff price
            "squareoff": round(price - squareoff, 2),
            # difference between stoploss price and price
            "stoploss": round(stoploss - price, 2),
            "quantity": qty
        }
        print(orderparams)
        orderId = obj.placeOrder(orderparams)
        print("The bracket order id is: {}".format(orderId))
    except Exception as e:
        print("Bracket Order placement failed: {}".format(e.message))
    finally:
        sleep(0.2)


def place_stoploss_limit_order(token, symbol, transaction_type, qty, trigger_price):
    if transaction_type == 'BUY':
        price = trigger_price + 0.05
    elif transaction_type == 'SELL':
        price = trigger_price - 0.05
    try:
        orderparams = {
            "variety": 'STOPLOSS',
            "tradingsymbol": symbol,
            "symboltoken": token,
            "transactiontype": transaction_type,
            "exchange": "NSE",
            "ordertype": "STOPLOSS_LIMIT",
            "producttype": "INTRADAY",
            "duration": "DAY",
            "price": round(price, 2),
            "triggerprice": round(trigger_price, 2),
            "quantity": qty,
        }
        print(orderparams)
        orderId = obj.placeOrder(orderparams)
        print("SL LIMIT order id is: {}".format(orderId))
    except Exception as e:
        print("SL LIMIT order placement failed: {}".format(e))
    finally:
        sleep(0.2)


def place_orders():
    t = dt.today()
    stocks = pd.read_csv('https://meanalgo.github.io/data/mean_reversion.csv', index_col=None)

    length = len(stocks['nsecode'])
    print(f'length is {length}.')
    if length == 0:
        print(f'no stocks to trade today, return')
        return

    eqs = pd.read_csv('angeleq.csv')

    for stock in stocks.itertuples():
        print(stock.nsecode)
        token = eqs[eqs['symbol'] == stock.nsecode + '-EQ']['token'].values[0]
        place_stoploss_limit_order(
            str(token), f"{stock.nsecode}-EQ", 'SELL', stock.qty, stock.trigger_price)
        sleep(0.2)
        place_stoploss_limit_order(
            str(token), f"{stock.nsecode}-EQ", 'BUY', stock.qty, stock.stoploss)


def cancel_old_orders():
    orders = obj.orderBook()
    orders_df = pd.DataFrame(orders['data'])
    if not orders_df.empty:
        orders_df = orders_df[['variety', 'ordertype', 'producttype', 'price', 'triggerprice',
                            'orderid', 'tradingsymbol', 'symboltoken', 'status', 'orderstatus', 'quantity']]
        print(orders_df)
        orders_df = orders_df[orders_df['producttype'] == 'INTRADAY']
        print(orders_df)
        for order in orders_df.itertuples():
            print(order)
            obj.cancelOrder(order.orderid, order.variety)


def print_order_book():
    orders = obj.orderBook()
    orders_df = pd.DataFrame(orders['data'])
    if not orders_df.empty:
        orders_df = orders_df[['variety', 'ordertype', 'producttype', 'transactiontype', 'price',
                            'triggerprice', 'orderid', 'tradingsymbol', 'symboltoken', 'status', 'orderstatus', 'quantity', 'ordertag']]
        print(orders_df)
        orders_df = orders_df[orders_df['producttype'] == 'INTRADAY']
        print(orders_df)


# run at 8:30 am
if __name__ == "__main__":
    place_orders()
    sleep(0.2)
    print_order_book()
    sleep(0.2)
